package com.gitlab.tulaon.votifier;

import com.gitlab.tulaon.votifier.api.Vote;
import com.gitlab.tulaon.votifier.api.VotifierProtocol2Encoder;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

@Slf4j
public class VotifierTester {

    private final VotifierTesterFrame votifierTesterFrame;
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private VotifierProtocol2Encoder votifierProtocol2Encoder = new VotifierProtocol2Encoder();

    public VotifierTester() {
        this.votifierTesterFrame = new VotifierTesterFrame();
    }

    public static void main(String[] args) {
        new VotifierTester().showFrame();
    }

    private void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            log.info("Could not set LookAndFeel", e);
        }
    }

    private void showFrame() {
        setLookAndFeel();
        JFrame frame = new JFrame("VotifierTester");
        frame.pack();
        frame.setContentPane(votifierTesterFrame.getVotifierTesterPanel());
        frame.setResizable(true);
        frame.setSize(400, 207);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);

        votifierTesterFrame.getSendVotifierPacketButton().addActionListener(e -> sendVote());
    }

    private void sendVote() {
        VoteContentPanel content = votifierTesterFrame.getVoteContentPanel();
        Vote v = Vote.builder()
                .withUsername(content.getUser().getText())
                .withServiceName(content.getService().getText())
                .withTimeStamp(System.currentTimeMillis())
                .withAddress("127.0.0.1")
                .build();
        PacketSender sender = new PacketSender(v, content.getHost().getText(), content.getPort().getText(), content.getToken().getText(), votifierProtocol2Encoder);
        executorService.submit(sender);
    }
}
