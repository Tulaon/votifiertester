package com.gitlab.tulaon.votifier;

import com.gitlab.tulaon.votifier.api.Vote;
import com.gitlab.tulaon.votifier.api.VotifierProtocol2Encoder;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
public class PacketSender implements Runnable {
    private final Vote vote;
    private final String host;
    private final String port;
    private final String token;
    private final VotifierProtocol2Encoder votifierProtocol2Encoder;

    @Override
    public void run() {
        try {
            @Cleanup Socket s = new Socket(host, Integer.parseInt(port));
            String[] result = readGreeting(s.getInputStream());
            log.info("Got Greeting: {}", String.join(" ", result).replace("\n", ""));
            if (result.length == 3 && result[1].equalsIgnoreCase("2")) {
                vote.setChallenge(result[2].replace("\n", ""));
                sendVote(s.getOutputStream());
                readResponse(s.getInputStream());
            } else {
                if (result.length != 3) {
                    log.error("Greeting to short. '{}'", String.join(" ", result));
                }
                if (!result[1].equalsIgnoreCase("2")) {
                    log.error("Server is not a V2 Server");
                }
            }
        } catch (IOException e) {
            log.error("An error occured while trying to send vote", e);
        }
    }

    private void readResponse(InputStream inputStream) throws IOException {
        int readAttempts = 0;
        while (readAttempts < 3) {
            if (inputStream.available() > 0) {
                byte[] buf = new byte[inputStream.available()];
                inputStream.read(buf, 0, inputStream.available());
                log.info("Vote Response: {}", new String(buf).replace("\n", ""));
                return;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                log.trace("Interrupted", e);
            }
            readAttempts++;
        }
        log.warn("Server did not send a response");
    }

    private String[] readGreeting(InputStream in) throws IOException {
        int readAttempts = 0;
        while(readAttempts < 3) {
            if(in.available() > 0) {
                byte[] greeting = new byte[in.available()];
                in.read(greeting,0,in.available());
                return new String(greeting).split(" ");
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                log.trace("Interrupted", e);
            }
            readAttempts++;
        }
        return new String[0];
    }


    private void sendVote(OutputStream out) throws IOException {
        Optional<String> msg = votifierProtocol2Encoder.encode(vote, token);
        if (msg.isPresent()) {
            ByteBuffer buf = ByteBuffer.allocate(msg.get().length() + 4);
            buf.putShort((short) 0x733A);
            buf.putShort((short) msg.get().length());
            buf.put(msg.get().getBytes());
            log.info("Send Vote: {}", msg.get());
            out.write(buf.array());
            out.flush();
        } else {
            log.error("Encoded Vote missing... ");
        }
    }
}
