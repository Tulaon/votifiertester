package com.gitlab.tulaon.votifier.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Optional;

import static java.nio.charset.StandardCharsets.*;

@Slf4j
public class VotifierProtocol2Encoder {
    private Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    public Optional<String> encode(Vote vote, String token) {
        try {
            return Optional.of(encodedVote(vote, token));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            log.error("Could not encode payload", e);
        }
        return Optional.empty();
    }

    private String encodedVote(Vote vote, String token) throws NoSuchAlgorithmException, InvalidKeyException {
        String payload = gson.toJson(vote) + "\n";
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(token.getBytes(UTF_8), "HmacSHA256"));
        VoteWrapper wrapper = new VoteWrapper(payload, Base64.getEncoder().encodeToString((mac.doFinal(payload.getBytes(UTF_8)))));
        return gson.toJson(wrapper)+"\n";
    }
}
