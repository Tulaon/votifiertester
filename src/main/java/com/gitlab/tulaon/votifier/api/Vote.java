package com.gitlab.tulaon.votifier.api;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class Vote {
    private String serviceName;
    private String username;
    private String address;
    private long timestamp;
    @Setter
    private String challenge;

    public static VoteBuilder builder() {
        return new VoteBuilder();
    }

    public static class VoteBuilder {
        private String serviceName;
        private String username;
        private String address;
        private String challenge;
        private long timestamp;

        public VoteBuilder withServiceName(String serviceName) {
            this.serviceName = serviceName;
            return this;
        }

        public VoteBuilder withUsername(String username) {
            this.username = username;
            return this;
        }

        public VoteBuilder withAddress(String address) {
            this.address = address;
            return this;
        }


        public VoteBuilder withChallenge(String challenge) {
            this.challenge = challenge;
            return this;
        }

        public VoteBuilder withTimeStamp(long timeStamp) {
            this.timestamp = timeStamp;
            return this;
        }

        public Vote build() {
            return new Vote(serviceName, username, address, timestamp,challenge);
        }
    }
}
