package com.gitlab.tulaon.votifier.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VoteWrapper {
    private String payload;
    private String signature;
}
