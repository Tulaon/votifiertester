package com.gitlab.tulaon.votifier.api;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class VoteTest {


    @Test
    void basicUseCase() {
        long timeStamp = System.currentTimeMillis();
        String challenge = "my-awesome-challenge";
        String serviceName = "testserver";
        String username = "votifier";
        String address = "127.0.0.1";

        Vote vote = Vote.builder()
                .withAddress(address)
                .withTimeStamp(timeStamp)
                .withUsername(username)
                .withServiceName(serviceName)
                .withChallenge(challenge)
                .build();

        Assertions.assertThat(vote.getAddress()).isEqualTo(address);
        Assertions.assertThat(vote.getChallenge()).isEqualTo(challenge);
        Assertions.assertThat(vote.getServiceName()).isEqualTo(serviceName);
        Assertions.assertThat(vote.getUsername()).isEqualTo(username);
        Assertions.assertThat(vote.getTimestamp()).isEqualTo(timeStamp);
    }
}