package com.gitlab.tulaon.votifier.api;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class VotifierProtocol2EncoderTest {

    @Test
    void encode() {
        VotifierProtocol2Encoder classToTest = new VotifierProtocol2Encoder();
        Optional<String> encode = classToTest.encode(
                new Vote(
                        "votifier",
                        "testuser",
                        "127.0.0.1",
                        1578770064815L,
                        "jiks1m07pnth96h6juthom5gev"
                ),
                "uicnu3nkhji8eire85j71u2gsm");
        Assertions.assertThat(encode).isPresent();
        String expected = "{\"payload\":\"{\\\"serviceName\\\":\\\"votifier\\\",\\\"username\\\":\\\"testuser\\\",\\\"address\\\":\\\"127.0.0.1\\\",\\\"timeStamp\\\":1578770064815,\\\"challenge\\\":\\\"jiks1m07pnth96h6juthom5gev\\\"}\\n\",\"signature\":\"F5NPCaJk722jWDKfZqXqIF2zznxQoWqg9WBO+nyvipc=\"}";
        Assertions.assertThat(encode.get()).isEqualToIgnoringCase(expected);
    }
}