package com.gitlab.tulaon.votifier;

import com.gitlab.tulaon.votifier.api.Vote;
import com.gitlab.tulaon.votifier.api.VotifierProtocol2Encoder;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@Slf4j
class PacketSenderTest {

    private VotifierProtocol2Encoder encoder = spy(new VotifierProtocol2Encoder());


    private String serverResponse = "{\"payload\":\"{\\\"serviceName\\\":\\\"votifier\\\",\\\"username\\\":\\\"testuser\\\",\\\"address\\\":\\\"127.0.0.1\\\",\\\"timestamp\\\":1578770064815,\\\"challenge\\\":\\\"my-awesome-challenge\\\"}\\n\",\"signature\":\"e4Tfss7YDdGQPNoKnsv6tkncb62pQEHKGOX6s15ufQ4=\"}";

    @Test
    void basicUseCase() throws Exception {

        Vote testVote = new Vote("votifier", "testuser", "127.0.0.1", 1578770064815L, "jiks1m07pnth96h6juthom5gev");


        TestServer server = new TestServer();
        PacketSender classToTest = new PacketSender(testVote, "127.0.0.1", String.valueOf(server.getPort()), "uicnu3nkhji8eire85j71u2gsm", encoder);
        Thread serverThread = new Thread(server);
        serverThread.start();

        Thread runnerThread = new Thread(classToTest);
        runnerThread.start();
        runnerThread.join();
        serverThread.join();

        verify(encoder).encode(eq(testVote), eq("uicnu3nkhji8eire85j71u2gsm"));
        Assertions.assertThat(server.getServerContent()).isEqualTo(serverResponse);
    }

    public class TestServer implements Runnable {
        ServerSocket serverSocket;
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        public TestServer() throws IOException {
            this.serverSocket = new ServerSocket(0);
        }

        public int getPort() {
            return serverSocket.getLocalPort();
        }

        public String getServerContent() {
            byte[] original = out.toByteArray();
            return new String(Arrays.copyOfRange(original, 6, original.length));
        }

        public void start() {
            try {
                Socket s = serverSocket.accept();
                s.getOutputStream().write("VOTE 2 my-awesome-challenge".getBytes());
                s.getOutputStream().flush();
                BufferedReader reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
                out.write(reader.readLine().getBytes());
                s.getOutputStream().write("{\"status\": \"ok\"}".getBytes());
                s.getOutputStream().flush();
                Thread.sleep(100);
                s.close();
                serverSocket.close();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            start();
        }
    }
}